import os
from selenium import webdriver
from selenium.webdriver.common.by import By
import time
import math
from selenium.webdriver.support.ui import Select

def calc(x):
 return str(math.log(abs(12*math.sin(int(x)))))    #Формуля
    
try:
    link = "http://suninjuly.github.io/alert_accept.html"
    browser = webdriver.Chrome()
    browser.get(link)
    button = browser.find_element(By.CSS_SELECTOR, "[type='submit']") #Ищем кнопку
    button.click()
    confirm = browser.switch_to.alert
    confirm.accept()
    x_element = browser.find_element(By.ID, 'input_value') #Ищим цифру из тега
    x = x_element.text # Присваеваем заначение найденной цифры 'x'
    y = calc(x) #Подставялем 'x' в формулу и присаеваем 'у' результаты формулы
    print ("у=",y) # Для себя вывожу значения 'y'
    print ("x=",x) # 'x'
    input = browser.find_element(By.CSS_SELECTOR, "[class='form-control']") #Ищем поле ввода
    input.send_keys(y)
    button2 = browser.find_element(By.CSS_SELECTOR, "[class='btn btn-primary']")   #Ищем кнопку
    button2.click()
   
   
finally:
    # успеваем скопировать код за 30 секунд
    time.sleep(5)
    # закрываем браузер после всех манипуляций
    browser.quit()

# не забываем оставить пустую строку в конце файла
