from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
import math

def calc(x):
 return str(math.log(abs(12*math.sin(int(x)))))    #Формуля

browser = webdriver.Chrome()

browser.get("http://suninjuly.github.io/explicit_wait2.html")

element = WebDriverWait(browser, 12).until(
        EC.text_to_be_present_in_element((By.XPATH, '//div/h5[2]'),
         "$100")) # Ждем когнда тест элементва появиться $100
button = browser.find_element(By.ID, 'book').click()
label = browser.find_element(By.XPATH, "//label/span[2]") #Ищим первую цифру
x = label.get_attribute("innerHTML") #Забераем значения первой цифры
s = calc(x)
print ("s=",s) # # Выод в консоль знасчение "s"
print ("x=",x) # Выод в консоль знасчение "x"
input = browser.find_element(By.CSS_SELECTOR, "[class='form-control']")#Ищем поле ввода
input.send_keys(s) #Вводим в поле значение y
button2 = browser.find_element(By.CSS_SELECTOR, "[type='submit']").click()   #Ищем кнопку,нажимаем кнопку

